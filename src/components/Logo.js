import React from 'react';

function Logo(props) {
  return (
    <div className='logo'>
      <img src="./logo192.png" alt="Logo" />
      <h3>Countries</h3>
    </div>
  );
}

export default Logo;