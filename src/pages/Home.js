import React from 'react';
import Countries from '../components/Countries';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

function Home(props) {
  return (
    <div>
      <Logo/>
      <Navigation/>
      <Countries/>
    </div>
  );
}

export default Home;