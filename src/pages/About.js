import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

function About(props) {
  return (
    <div>
      <Logo/>
      <Navigation />
      <h1>A propos</h1>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus
        facilis est in illo quos consectetur, rerum dolorum tempora eius
        aspernatur molestias nisi nesciunt odit quisquam ut praesentium
        perferendis quis eveniet.
      </p>
      <br />
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit nam
        totam animi perferendis eligendi mollitia, perspiciatis non debitis vel
        ipsa magnam deleniti sit recusandae numquam. Dolor eaque doloremque
        debitis quam!
      </p>
    </div>
  );
}

export default About;
